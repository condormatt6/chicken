import 'package:flutter/material.dart';

import 'constants.dart';

class MyDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      padding: EdgeInsets.zero,
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 7.0),
              height: 80.0,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/ratatouille.jpg'),
                ),
                shape: BoxShape.circle,
              ),
            ),
            const Text(
              'Chicken',
              style: TextStyle(
                  color: Colors.white, fontFamily: 'Pacifico', fontSize: 30.0),
            ),
          ],
        ),
      ),
      decoration: const BoxDecoration(color: kPrimaryColor),
    );
  }
}
