import 'package:chicken/component/service.dart';
import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF8e7849);

List<Service> service = [
  Service(nom: "Poulet Entier", imagePath: "assets/images/ratatouille.jpg"),
  Service(nom: "Poulet Deplumée", imagePath: "assets/images/logo.png")
];
