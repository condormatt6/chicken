import 'package:chicken/component/constants.dart';
import 'package:chicken/component/roundedButtom.dart';
import 'package:chicken/localization/demo_localization.dart';
import 'package:chicken/views/pinConfirmation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Flexible(
                  child: Hero(
                    tag: 'logo',
                    child: Container(
                      height: 200.0,
                      child: Image.asset('assets/images/logo.png'),
                    ),
                  ),
                ),
                Flexible(
                  child: SizedBox(
                    height: 48.0,
                  ),
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  controller: _controller,
                  decoration: InputDecoration(
                    hintText: DemoLocalization.of(context)
                        .getTranslatedValue('phone_hint'),
                    hintStyle: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    filled: true,
                    isDense: true,
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      // borderSide: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white, width: 2),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    prefixIcon: InkWell(
                      onTap: () {},
                      child: Container(
                        padding: const EdgeInsets.only(left: 10.0),
                        width: 80,
                        child: Row(
                          children: [
                            const Icon(
                              Icons.arrow_drop_down,
                              size: 29,
                              color: Colors.white,
                            ),
                            Text(
                              '+228',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: SizedBox(
                    height: 24.0,
                  ),
                ),
                RoundedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PinConfirmation(
                                phone: _controller.text,
                              )),
                    );
                  },
                  title:
                      DemoLocalization.of(context).getTranslatedValue('next'),
                  coulor: kPrimaryColor,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
