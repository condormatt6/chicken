import 'package:chicken/component/Language.dart';
import 'package:chicken/component/constants.dart';
import 'package:chicken/component/drawerHeader.dart';
import 'package:chicken/localization/demo_localization.dart';
import 'package:chicken/main.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final String title;
  HomePage({this.title});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void _changeLanguage(Language language) {
    Locale _temp;
    switch (language.languageCode) {
      case 'en':
        _temp = Locale(language.languageCode, 'US');
        break;
      case 'fr':
        _temp = Locale(language.languageCode, 'FR');
        break;
      case 'de':
        _temp = Locale(language.languageCode, 'DE');
        break;
      default:
        _temp = Locale(language.languageCode, 'US');
    }
    MyApp.setLocale(context, _temp);
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(widget.title),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
          )
        ],
      ),
      drawer: Container(
        width: MediaQuery.of(context).size.width / 2 + 50.0,
        child: Drawer(
            child: SingleChildScrollView(
          child: Column(
            children: [
              //myDrawer
              MyDrawerHeader(),
              MyDrawerList(),
            ],
          ),
        )),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0, left: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                DemoLocalization.of(context).getTranslatedValue('category'),
                style: TextStyle(
                    color: kPrimaryColor,
                    fontFamily: 'Pacifico',
                    fontSize: 20.0),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                height: MediaQuery.of(context).size.width / 2,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: service.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        print(service[index].nom);
                      },
                      child: Card(
                        elevation: 1.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            side: BorderSide(color: kPrimaryColor)),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Center(child: Text(service[index].nom)),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(service[index].imagePath),
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Text(
                DemoLocalization.of(context).getTranslatedValue('new'),
                style: TextStyle(
                    color: kPrimaryColor,
                    fontFamily: 'Pacifico',
                    fontSize: 20.0),
              ),
              Padding(padding: EdgeInsets.only(top: 5.0)),
              GridView.builder(
                  shrinkWrap: true,
                  itemCount: 6,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount:
                          orientation == Orientation.portrait ? 2 : 3,
                      mainAxisSpacing: 8,
                      crossAxisSpacing: 8,
                      childAspectRatio:
                          (orientation == Orientation.portrait) ? 0.7 : 0.8),
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 10.0,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Padding(padding: EdgeInsets.only(top: 4)),
                          CircleAvatar(
                            radius: 50.0,
                            backgroundImage:
                                AssetImage('assets/images/ratatouille.jpg'),
                          ),
                          Text(
                            'Poulet',
                            style: TextStyle(fontSize: 15),
                          ),
                          Text(
                            'Sans Os',
                            style: TextStyle(color: Colors.black38),
                          ),
                          Text(
                            '\$ 28.89',
                            style: TextStyle(color: kPrimaryColor),
                          ),
                          RaisedButton(
                            color: kPrimaryColor,
                            onPressed: () {},
                            child: Text(
                              'Add',
                              style: TextStyle(color: Colors.white),
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}

Widget MyDrawerList() {
  return Container(
    padding: EdgeInsets.only(
      top: 5,
    ),
    child: Column(
      children: [
        MenuList(
          icondat: Icons.dashboard,
          name: 'Dashboard',
          coulor: kPrimaryColor,
        ),
        MenuList(
          icondat: Icons.dashboard,
          name: 'Dashboard',
          coulor: Colors.red,
        ),
      ],
    ),
  );
}

class MenuList extends StatelessWidget {
  IconData icondat;
  String name;
  Color coulor;
  MenuList({this.icondat, this.name, this.coulor});
  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          print('Hello');
        },
        child: Padding(
          padding: EdgeInsets.fromLTRB(10, 2, 10, 0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Icon(
                      icondat,
                      size: 20,
                      color: coulor,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      name,
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
              Divider(
                height: 20.0,
                color: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}
